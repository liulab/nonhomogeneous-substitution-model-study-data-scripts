from pathlib import Path

def parse_result(result_path):
    if type(result_path) != Path:
        result_path = Path(result_path)
    assert result_path.is_file()
