#!/bin/bash
if [ -z $1 ]; then
    echo "no directory supplied"
    exit 1;
fi

cd $1 || { echo "no such directory $1"; exit 1; }
source parameters.sh # this will probably break - sorry. 
# For the scripts, nhts (wrapper for baseml search) 
# should be in the path.

# ProgressBar 0 $num_replicates "  Estimation: "

for rep in $(seq 1 ${num_replicates}); do
    pushd replicate_$rep >/dev/null
    sbatch ../../files/slurm/rooted-tree-estimation.sb sequence_TRUE.phy TRUE
    sbatch ../../files/slurm/rooted-tree-estimation.sb sequence_MAFFT.phy MAFFT 
    sbatch ../../files/slurm/rooted-tree-estimation.sb sequence_MUSCLE.phy MUSCLE
    sbatch ../../files/slurm/rooted-tree-estimation.sb sequence_FSA.phy FSA
    sbatch ../../files/slurm/rooted-tree-estimation.sb sequence_CLUSTALO.phy CLUSTALO
    sbatch ../../files/slurm/rooted-tree-estimation.sb sequence_CLUSTALW.phy CLUSTALW
    popd >/dev/null
done
echo ""
