# Nonhomogeneous substitution model study scripts and data

Contains processed data and relevant scripts used in the manuscript.

Scripts - Contains wrapper for baseml (nhts) to do topological estimation under nonhomogeneous substitution model as well as slurm scripts used for estimating trees in the simulation study.

## 1. Dataset
### 1.1 Simulation study dataset
The ./simulations/ folder contains data for the simulation study experiments.
| Model condition | Number of taxa | Tree height | Insertion/deletion probability |
|:---------------:|:--------------:|:-----------:|:------------------------------:|
|       10.A      |       10       |     0.47    |              0.13              |
|       10.B      |       10       |     0.7     |               0.1              |
|       10.C      |       10       |     1.2     |              0.06              |
|       10.D      |       10       |     2.0     |              0.031             |
|       10.E      |       10       |     4.4     |              0.013             |
|       20.A      |       20       |     0.47    |              0.13              |
|       20.B      |       20       |     0.7     |               0.1              |
|       20.C      |       20       |     1.2     |              0.06              |
|       20.D      |       20       |     2.0     |              0.031             |
|       20.E      |       20       |     4.4     |              0.013             |

Each replicate folder contains the following files:


### 1.2 Empirical study dataset
The ./empirical/ folder contains data for the empirical study analyses.

The ./empirical/per-gene.tar.gz archive contains contains per gene analyses.  
Each folder OG00XXXXX contains the following files:
- **unaligned.fasta**: Unaligned orthogroup sequences
- **<alignment method>.fasta**: estimated alignment
- **singleshift_<alignment method>**: Results of single-shift search

The ./empirical/concatenated.tar.gz archive contains the following files:
- **<alignment method>.phylip**: Alignment for respective method
- **singleshift-<alignment method>**: Results of single-shift search

## 2. Software
The ./scripts/ directory contains software for searching, as well as 
### 2.1 Requirements
A conda environment manager (anaconda, miniconda, etc) is required.
### 2.2 Setup
To set up the environment for nonhomogeneous tree search:  
`conda env create -f scripts/baseml-search/environment.yml`
./scripts/nhts --help for info.
