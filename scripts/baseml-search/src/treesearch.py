from pathlib import Path
from Bio import AlignIO, Phylo
from Bio.Phylo.TreeConstruction import NNITreeSearcher, DistanceCalculator, DistanceTreeConstructor
from io import StringIO
import logging
import numpy as np
import random
import ete3
import subprocess

from utils import baseml
from utils import rearrange

logger = logging

def bipart_name(e):
    return ({x.name for x in e[0]}, {x.name for x in e[1]})

def tree_as_bipartitions(tree, rooted=False):
    if rooted:
        return {frozenset((frozenset(x.name for x in e[0]),frozenset({x.name for x in e[1]}.union({"ROOT"})))) for e in tree.get_edges()}
    else:
        return {frozenset((frozenset(x.name for x in e[0]),frozenset(x.name for x in e[1]))) for e in tree.get_edges()}

def fill_baseml_template(ctl_template, replacements):
    result = ctl_template
    for this, with_this in replacements:
        result = result.replace(this, with_this)
    return result

def nearest_neighbors(ete3tree, is_unrooted=True):
    #ete3tree = ete3.Tree(tree_string)
    if is_unrooted:
        ete3tree.resolve_polytomy()
        bio_tree = Phylo.read(StringIO(ete3tree.write(format=9)),format='newick')
        bio_neighbors = NNITreeSearcher._get_neighbors(None, bio_tree)
        all_neighbors = [ete3.Tree(t.format('newick')) for t in bio_neighbors]
        for t in all_neighbors:
            t.unroot()
        return all_neighbors
    else:
        nx_tree = rearrange.ete_to_networkx(ete3tree)
        return [ete3.Tree(rearrange.networkx_to_newick(x)) for x in rearrange.networkx_NNI_neighborhood(nx_tree)]

def subtree_prune_regraft(tree, subtree, regraft_node):
    t = tree.copy()
    st_node = t.get_common_ancestor([x.name for x in subtree.get_leaves()])
    rg_node = t.get_common_ancestor([x.name for x in regraft_node.get_leaves()])
    if regraft_node.is_leaf():
        rg_node = t.get_leaves_by_name(regraft_node.name)[0]
    if subtree.is_leaf():
        st_node = t.get_leaves_by_name(subtree.name)[0]
    rg_parent = rg_node.up
    st_parent = st_node.up

    subtree_sister = st_node.get_sisters()[0]

    st_node.detach()

    regraft_parent = rg_node.up
    new_node = ete3.TreeNode()
    regraft_parent.add_child(new_node)
    rg_node.detach()
    new_node.add_child(rg_node)
    new_node.add_child(st_node)

    sister_children = subtree_sister.get_children()
    if subtree_sister.is_leaf():
        st_parent.name = subtree_sister.name
    
    for sc in sister_children:
        sc.detach()
        subtree_sister.up.add_child(sc)
    subtree_sister.detach()
    
    return t

def spr(original_tree):
    tree = original_tree.copy()
    tree.resolve_polytomy()
    # iterate through subtrees
    subtree_iter = tree.traverse()
    next(subtree_iter)
    st_rg_pairs = [(st,rg) for st in tree.traverse() for rg in tree.traverse()]
    random.shuffle(st_rg_pairs)
    # for subtree in subtree_iter:
    #    for rg_node in tree.traverse():
    for subtree, rg_node in st_rg_pairs:
        if rg_node.is_root():
            continue
        elif rg_node == subtree:
            continue
        elif rg_node == subtree.up:
            continue
        elif rg_node in subtree.get_descendants():
            continue
        elif rg_node == subtree.get_sisters()[0]:
            continue
        else:
            yield subtree_prune_regraft(tree, subtree, rg_node)
        
def random_tree(alignment, unroot=True):
    tree = ete3.Tree()
    taxa = [x.name for x in alignment]
    tree.populate(len(taxa), names_library=taxa)
    if unroot:
        tree.unroot()
    return tree

def nj_tree(alignment, unrooted=True):
    calculator = DistanceCalculator('identity')
    constructor = DistanceTreeConstructor(calculator, 'nj')
    bio_tree = constructor.build_tree(alignment)
    result = ete3.Tree(bio_tree.format('newick').replace('Inner', ''))

    if unrooted:
        result.unroot()
    else:
        result.set_outgroup((result.get_midpoint_outgroup()))
    return result

def best_result(results):
    best_result = None
    best_log_likelihood = float('-inf')
    for result in results:
        if result and result['log likelihood'] > best_log_likelihood:
            best_log_likelihood = result['log likelihood']
            best_result = result
    return best_result

def get_shift_partitions(tree, k=1):
    # So far I've only implemented this for bipartitions (k=1 shifts)
    # I believe all the ways you can choose k nodes tells you how
    assert k==1
    sps = set()
    nodeset = set(tree.traverse())
    for node in tree.traverse():
        if node.is_root():
            continue
        p1 = set(node.get_descendants())
        p1.add(node)
        p1 = frozenset(p1)
        p2 = frozenset(nodeset - p1)
        sps.add(frozenset((p1,p2)))
    return sps


def single_shift_assignments(input_tree):
    tree = input_tree.copy()
    shift_partitions = get_shift_partitions(tree)
    for node in tree.traverse():
        node.original_name = node.name

    model_assignments = []
    for sp in shift_partitions:
        for modelnum, nodes in enumerate(sp, start=1):
            for node in nodes:
                if node.is_root():
                    root_name = f"#{modelnum}"
                node.name = f"{node.original_name}#{modelnum}"
        model_assignments.append(tree.write(format=8).replace(";",f"{root_name};"))
    return model_assignments

def run_baseml(tree, output_path, ctl_template, writetree=lambda x : x.write(format=9), cleanup="delete"):
    output_path.mkdir(exist_ok=True, parents=True)
    control_path = Path(f"{output_path}/baseml.ctl")
    result_path = Path(f"{output_path}/RESULT")
    tree_path = Path(f"{output_path}/tree")
    log_path = Path(f"{output_path}/LOG")
    replacements = [
        ("#TREEFILE",  tree_path.name),
        ("#OUTPUTFILE", result_path.name),
    ]
    baseml_control = fill_baseml_template(ctl_template, replacements)
    with open(tree_path, 'w') as fo:
        fo.write(f"{len(tree)} 1\n")
        fo.write(writetree(tree))
        fo.write('\n')
    with open(control_path, 'w') as fo:
        fo.write(baseml_control)

    proc_args = ["baseml", f"baseml.ctl"]
    with open(log_path, 'w') as fo:
        baseml_proc = subprocess.Popen(proc_args, cwd=output_path, stdout=fo)
        baseml_proc.wait()
    # print(result_path)
    result = best_result(baseml.parse_baseml_result(result_path))
    if not result:
        result = baseml.parse_baseml_log(log_path)
        if result:
            # try reading the treefile
            with open(tree_path, 'r') as fi:
                tf_lines = fi.readlines()
            try:
                if '#' in tf_lines[1]:
                    # NOTE: stupidly edge case edge case
                    # this is to handle a complication if the tree file
                    # uses a tree with a branch model for some reason, 
                    # e.g. ((A#2,B#2)#2,C#1)#1; 
                    # assumes every branch is assigned a branch model (including root)
                    # replace this with something that actually handles that
                    # this is already a fallback in case for some reason the
                    # logfile has a likelihood but the result file doesn't
                    tree_str = tf_lines[1].strip().replace('#', '[&&NHX:model=').replace(',','],').replace(')[', '])[').replace(';','];')
                else:
                    tree_str = tf_lines[1].strip()
                result['tree'] = ete3.Tree(tree_str)
            except:
                result = {}
    if result:
        result["Path"] = output_path
    return result

def run_single_shift_baseml(tree, output_path, ctl_template, writetree=lambda x: x.write(format=9), cleanup="delete"):
    model_assignments = single_shift_assignments(tree)
    best_info = None
    best_likelihood = float('-inf')
    output_path.mkdir(parents=True, exist_ok=True)
    for ix, model_tree in enumerate(model_assignments, start=1):
        sub_path = Path(f"{output_path}/single_shift_{ix}")
        sub_path.mkdir(exist_ok=True)
        result = run_baseml(tree, sub_path, ctl_template, writetree=lambda x: model_tree, cleanup=cleanup)
        if result and best_likelihood < result["log likelihood"]:
            best_info = result
            best_info["Path"] = sub_path
            best_likelihood = result["log likelihood"]
    if cleanup == "delete":
        for p in output_path.iterdir():
            if p != best_info["Path"]:
                subprocess.run(["rm", "-rf", p.absolute()])
    #elif cleanup == "compress":

    return best_info

def serial_search(args):

    if args.seed: # ehhh... untested. a half measure at best
        random.seed(args.seed)

    with open(args.template, 'r') as fi:
        ctl_template = "".join(fi.read())
    ctl_template = ctl_template.replace("#SEQFILE", str(args.seq.absolute()))
    
    alignment = AlignIO.read(args.seq, format='phylip-relaxed')
    if args.start == 'nj':
        best_tree = nj_tree(alignment, unrooted=(not args.rooted))
    elif args.start == 'random':
        best_tree = random_tree(alignment, unroot=(not args.rooted))
    elif isinstance(args.start, Path):
        best_tree = ete3.Tree(str(args.start))
        if args.rooted and len(best_tree.children) == 3:
            best_tree.set_outgroup(best_tree.get_midpoint_outgroup())
    else:
        raise NotImplementedError(f"Starting strategy '{args.starting_tree}' not implemented") 
    del alignment # was only used to get an initial estimate
    
    breadth_flag = False
    if args.rearrange == "NNI":
        rearrange = lambda x : nearest_neighbors(x, is_unrooted=(not args.rooted))
    elif args.rearrange == "NNI_B":
        rearrange = lambda x : nearest_neighbors(x, is_unrooted=(not args.rooted))
        breadth_flag = True
    elif args.rearrange == "SPR":
        if args.rooted:
            raise NotImplementedError("Rooted SPR moves not implemented.")
        rearrange = spr
    else:
        raise NotImplementedError(f"Rearrangement strategy '{args.rearrange}' not implemented")
    
    if args.likelihood_method == "singleshift":
        lh_calc = run_single_shift_baseml
    elif args.likelihood_method == "full":
        lh_calc = run_baseml

    cleanup = args.cleanup

    stepnum = 0
    visited_trees = {frozenset(tree_as_bipartitions(best_tree, rooted=args.rooted))}
    opath = Path(f"{args.output}/step_{stepnum}")
    result = lh_calc(best_tree, opath, ctl_template, cleanup=cleanup)
    best_info = result
    if result:
        best_likelihood = result["log likelihood"]
    else:
        best_likelihood = float('-inf')
    initial_likelihood = best_likelihood
    # cursed
    ndigits = len(str(args.max_iter))
    total_topos = 1

    logger.info("step\tlikelihood\t# topologies evaluated\tcurrent best topology")
    logger.info(f"{str(stepnum).zfill(ndigits)}\t{best_likelihood}\t{1}\t{best_tree.write(format=9)}")
    iter_num = 1
    while iter_num <= args.max_iter:
        topologies = 0
        # strategy is to visit the neighbors of the highest likelihood tree visited
        # to try different strategies probably change this
        prev_best_tree = best_tree
        neighbors = rearrange(best_tree)
        for neighbor in neighbors:
            # Check if this neighbor has been visited already
            bp_rep = frozenset(tree_as_bipartitions(neighbor, rooted=args.rooted))
            if bp_rep not in visited_trees:
                topologies += 1
                total_topos += 1
                stepnum += 1
                opath = Path(f"{args.output}/step_{stepnum}")
                visited_trees.add(bp_rep)
                result = lh_calc(neighbor, opath, ctl_template, cleanup=cleanup)
                logger.debug(f"\t{result['log likelihood'] if 'log likelihood' in result else 'NaN'}\t\t{neighbor.write(format=9)}")
                if result and result["log likelihood"] > best_likelihood:
                    best_likelihood = result["log likelihood"]
                    best_tree = result["tree"]
                    best_info = result
                    if not breadth_flag:
                        break
                else:
                    if cleanup == "delete":
                        # how to handle suboptimal results
                        # in this case, just delete them so they don't pollute the filesystem
                        subprocess.run(["rm", "-rf", opath.absolute()])
        if prev_best_tree.compare(best_tree, unrooted=(not args.rooted))["norm_rf"] == 0:
            break
        
        logger.info(f"{str(iter_num).zfill(ndigits)}\t{best_likelihood}\t{topologies}\t{best_tree.write(format=9)}")
        iter_num += 1

    # cleanup
    if cleanup == "delete":
        for p in args.output.iterdir():
            if not best_info["Path"].is_relative_to(p):
                subprocess.run(["rm", "-rf", p.absolute()])
    print(f"best topology: {best_tree.write(format=9)}")
    print(f"log likelihood: {best_likelihood}")
    logger.info(f"# topologies evaluated: {total_topos}")
    logger.info(f"Change in likelihood: {best_likelihood - initial_likelihood}")
    return best_info["Path"]
