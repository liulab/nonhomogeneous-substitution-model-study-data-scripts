Tree file : INDELible V1.03 (best viewed cut and paste in e.g. excel)

N.B. Simulation blocks with no random trees will have their trees printed for the first replicate only.

FILE	TREE	NTAXA	REP	PART	LENGTH	DEPTH	MAX PAIRWISE DISTANCE	TREE STRING

outputname	mytree	4	1	1	1.51413	0.47	2	((2:0.37362,3:0.37362):0.0963796,(4:0.20051,1:0.20051):0.26949);
