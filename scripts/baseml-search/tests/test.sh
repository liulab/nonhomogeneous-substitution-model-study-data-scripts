#!/bin/bash
eval "$(conda shell.bash hook)"
conda activate nhts-env

if [ -z $1 ]; then
    echo "No test directory supplied"
    exit -1
elif [ ! -d $1 ]; then
    echo "The test directory $1 doesn't exist! Exiting..."
    exit -1
fi

cd $1

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
bindir=$( dirname $SCRIPT_DIR )
bindir=$( dirname $bindir)

for rep in replicate_*
do
    pushd $rep >/dev/null
    ../../../nhts -t ../../template.ctl -s sequence_TRUE.phy -o SINGLETREE -M 0 -S model_tree
    # ../../../nhts -t ../../template.ctl -s sequence_TRUE.phy -o SPR_TRUE -R SPR
    popd >/dev/null
done

conda deactivate
