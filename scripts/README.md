# nhts
nhts (non-homogeneous tree search) is the wrapper script for baseml.
## Setup
To use nhts, anaconda is required.
`conda env create -f baseml-search/environment.yml`

## Usage
See `nhts --help`

# files
## baseml 
Template control files for nhts to use
## indelible
Template control files used to generate simulated data with INDELible
## slurm
Slurm job scripts used for estimating trees.
