import re

from pathlib import Path

# horrific

# honestly how have i made it this far without knowing how to make parsers

INDELIBLE_COMMANDS = {
    "TYPE" : {
    },
    "SETTINGS" : {
        "ancestralprint",
        "output",
        "phylipextension",
        "nexusextension",
        "fastaextension",
        "randomseed",
        "printrates",
        "insertaslowercase",
        "markdeletedinsertions",
        "printcodonsasaminoacids",
        "fileperrep"
    },
    "MODEL" : {
        "submodel",
        "indelmodel",
        "indelrate",
        "geneticcode",
        "rates",
        "statefreq"
    },
    "TREE" : {
        "unrooted",
        "rooted",
        "seed",
        "treedepth",
        "treelength",
        "branchlengths",
        "maxdistance"
    },
    "BRANCHES" : {
    },
    "PARTITIONS" : {
    },
    "EVOLVE": {
    }
}

VALID_COMMANDS = re.compile('|'.join(INDELIBLE_COMMANDS.keys()))

"""
yuck
subject to change
expects a Path or a string to an actual path 
"""
def parse_indelible_control(control_file_path):
    if type(control_file_path) != Path:
        control_file_path = Path(control_file_path)
    assert control_file_path.is_file()
    blocks = []
    block = None
    with open(control_file_path, 'r') as fi:
        lines = fi.readlines()
    tokens = " ".join(lines).split()
    while tokens:
        token = tokens.pop(0)
        block_search = VALID_COMMANDS.search(token)
        if block_search:
            if block:
                blocks.append(block)
            block_type = block_search.group()
            block = (block_type, [])
        else:
            block[1].append(token)
    result = {vc : {} for vc in INDELIBLE_COMMANDS.keys()}
    for block in blocks:
        result[block[0]].update(parse_block(*block))
    return result

def parse_block(block_type, block):
    if block_type == 'TYPE':
        return parse_type(block)
    elif block_type == 'SETTINGS':
        return parse_settings(block)
    elif block_type == 'MODEL':
        return parse_model(block)
    elif block_type == 'TREE':
        return parse_tree(block)
    elif block_type == 'BRANCHES':
        return parse_branches(block)
    elif block_type == 'PARTITIONS':
        return parse_partitions(block)
    elif block_type == 'EVOLVE':
        return parse_evolve(block)
    else:
        return {}
    
def parse_type(block):
    result = {'sequence type': block[0], 'method' : block[1]}
    return result

def parse_settings(block):
    result = {}
    return result

def parse_model(block):
    name = block.pop(0)
    subcommand = re.compile('|'.join(['submodel', 'indelmodel', 'indelrate', 'geneticcode', 'rates', 'statefreq']))
    result = {name : {}}
    while block:
        token = block.pop(0)
        # bad dont do this
        if token == '[submodel]':
            result[name]['substitution model'] = block.pop(0)
            result[name]['substitution rates'] = []
            while block and not subcommand.search(block[0]):
                result[name]['substitution rates'].append(float(block.pop(0)))
        elif token == '[indelmodel]':
            result[name]['indel model'] = block.pop(0)
            result[name]['indel model parameters'] = []
            while block and not subcommand.search(block[0]):
                result[name]['indel model parameters'].append(block.pop(0))
        elif token == '[indelrate]':
            result[name]['indel rate'] = block.pop(0)
        elif token == '[geneticcode]':
            pass
        elif token == '[rates]':
            pass
        elif token == '[statefreq]':
            result[name]['base frequencies'] = []
            while block and not subcommand.search(block[0]):
                result[name]['base frequencies'].append(float(block.pop(0)))
    return result

def parse_tree(block):
    name = block.pop(0)
    result = {name : {'tree' : block.pop(0)}}
    while block:
        token = block.pop(0)
    return result

def parse_branches(block):
    name = block.pop(0)
    result = {name : {'branch model' : "".join(block)}}
    return result

def parse_partitions(block):
    name = block.pop(0)
    result = {name : {
            'tree name' : block[0].strip('['),
            'model name' : block[1],
            'sequence length' : block[2].strip(']')
        }
    }
    return result

def parse_evolve(block):
    result = {}
    return result

def string_to_nhx(tree_string):
    return tree_string.replace('#', '[&&NHX:model=').replace(',','],').replace(')[', '])[').replace(';','];')

def assign_model_to_branches(result_dict, partition):
    tree = ete3.Tree(result_dict['TREE'][partition['tree name']]['tree'])
    branch_models = ete3.Tree(string_to_nhx(result_dict['BRANCHES'][partition['model name']]['branch model']))
    for x,y in zip(tree.traverse(), branch_models.traverse()):
        x.add_feature('model', y.model)
    return tree

def read_INDELIble_models(control_path):
    result = parse_indelible_control(control_path)
    partition = list(result['PARTITIONS'].values())[0]
    return assign_model_to_branches(result, partition)
