import networkx as nx

def ete_to_networkx(ete_tree):
    T = nx.Graph()
    T.add_node("ROOT")
    T.add_node("INTERNAL_0")
    T.add_edge("ROOT", "INTERNAL_0")
    node_stack = [(ete_tree, "INTERNAL_0")]
    num = 1
    while node_stack:
        current_node, node_label = node_stack.pop(0)
        for child in current_node.children:
            if child.name:
                child_label = child.name
            else:
                child_label = f"INTERNAL_{num}"
                num += 1
            T.add_node(child_label)
            T.add_edge(node_label, child_label)
            node_stack.append((child, child_label))
    return(T)

def networkx_NNI(T, left, right):
    assert len(T.adj[left]) == len(T.adj[right]) == 3
    assert right in T.adj[left]
    T1 = T.copy()
    T2 = T.copy()
    
    left_adj = list(T.adj[left])
    left_adj.remove(right)
    
    right_adj = list(T.adj[right])
    right_adj.remove(left)
    
    T1.remove_edges_from([(left, left_adj[1]),(right, right_adj[0])])
    T2.remove_edges_from([(left, left_adj[1]),(right, right_adj[1])])
    
    T1.add_edges_from([(right, left_adj[1]), (left, right_adj[0])])
    T2.add_edges_from([(right, left_adj[1]), (left, right_adj[1])])
    
    return T1, T2

# Using this for rearranging rooted trees
def networkx_NNI_neighborhood(T):
    assert nx.is_tree(T)
    for x,y in T.edges:
        if len(T.adj[x]) == len(T.adj[y]) == 3:
            T1, T2 = networkx_NNI(T, x, y)
            yield T1
            yield T2

def _recursive_networkx_to_newick(T, node, visited):
    if len(T.adj[node]) == 1:
        return node
    else:
        unvisited_neighbors = list(set(T.adj[node]) - (visited))
        visited = visited.union(unvisited_neighbors)
        return f"({','.join(_recursive_networkx_to_newick(T, x, visited) for x in unvisited_neighbors)})"    

def networkx_to_newick(T):
    assert nx.is_tree(T)
    result = ""
    start = list(T.adj['ROOT'])[0]
    
    visited = {start, "ROOT"}
    
    return _recursive_networkx_to_newick(T, start, visited) + ';'
