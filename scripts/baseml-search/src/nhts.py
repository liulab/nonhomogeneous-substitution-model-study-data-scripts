#!/usr/bin/env python3
import argparse
import subprocess
import time

from pathlib import Path
from treesearch import serial_search, logger

FORMAT="%(levelname)s:\t%(message)s"

def valid_file(path_str):
    p = Path(path_str)
    if p.is_file():
        return p
    else:
        raise FileNotFoundError(f"could not find a file at {path_str}")

def valid_output(path_str):
    p = Path(path_str)
    if p.parent.exists():
        return p
    else:
        raise NotADirectoryError(f"{p.parent} is not a valid directory")

def valid_start_strategy(strat_str):
    valid_strats = {'random', 'nj', 'raxml', 'paml'}
    if strat_str in valid_strats:
        return strat_str
    elif Path(strat_str).is_file():
        return Path(strat_str)
    else:
        raise Exception(f"Not a valid starting strategy: {strat_str}")

def is_positive(x):
    ivalue = int(x)
    if ivalue < 1:
        raise argparse.ArgumentTypeError(f"Number of processes must be at least 1 (got {x})")
    return ivalue

def valid_rearrangement_strategy(strat_str):
    valid_strats = {'NNI', 'NNI_B', 'SPR'}
    if strat_str in valid_strats:
        return strat_str
    else:
        raise Exception(f"Not a valid tree rearrangement strategy: {strat_str}")

def valid_likelihood_calc(lh_str):
    valid_lh_strs = {"singleshift", "full"}
    if lh_str.lower() in valid_lh_strs:
        return lh_str.lower()
    else:
        raise Exception(f"Not a valid topology likelihood evaluation method")

def valid_logging_level(logging_str):
    valid_logging_levels = {'critical', 'error', 'warning', 'info', 'debug'}
    if logging_str.lower() in valid_logging_levels:
        return logging_str.lower()
    else:
        raise Exception(f"Not a valid logging level: {logging_str}")

def valid_cleanup(cleanup_str):
    valid_ways = {'delete', 'keep'}
    if cleanup_str.lower() in valid_ways:
        return cleanup_str.lower()
    else:
        raise Exception(f"Not a valid cleanup strategy: {cleanup_str}")

def make_parser():
    parser = argparse.ArgumentParser(
        description="Wrapper for tree search (serial version)"
    )
    parser.add_argument("-t", "--template", type=valid_file, help="baseml control file template path", required=True)
    parser.add_argument("-s", "--seq", type=valid_file, help="sequence path (PHYLIP format)", required=True)
    parser.add_argument("-o", "--output", type=valid_output, help="Directory to output results to", required=True)
    parser.add_argument("-z", "--seed", type=int, help="Seed PRNG")
    parser.add_argument("-S", "--start", type=valid_start_strategy, help="Starting tree strategy: 'random', 'nj', 'raxml', or 'paml'", default='nj') # for now...
    parser.add_argument("-R", "--rearrange", type=valid_rearrangement_strategy, help="""Strategy for rearranging trees: 'NNI', 'NNI_B', 'SPR'
NNI:   Nearest-neighbors interchange
NNI-B: Nearest-neighbors interchange where every neighbor of the best tree 
       topology has its likelihood evaluated before picking the best neighbor.
SPR:   Standard subtree pruning and regrafting""",
       default="NNI"
    )
    parser.add_argument("-M", "--max_iter", type=int, help="Maximum number of iterations", default=100)
    parser.add_argument("-L", "--likelihood_method", type=valid_likelihood_calc, help="Method for evaluating the likelihood of a topology: 'singleshift' or 'full'", default='singleshift')
    parser.add_argument("-l", "--logging", type=valid_logging_level, help="Logging level", default="info")
    parser.add_argument("-c", "--cleanup", type=valid_cleanup, help="How to handle intermediate files ('delete' or 'keep')", default="delete")
    parser.add_argument("-r", "--rooted", help="Enable rooted tree topologies", action="store_true")
    return parser

def main():
    parser = make_parser()
    parser.add_argument("-P", "--MPI", type=is_positive, default=1, help=argparse.SUPPRESS)#help="number of MPI processes. If set to 1, program executes serially")
    
    try:
        args = parser.parse_args()
    except Exception as e:
        print(e)
        parser.print_help()
        exit(0)

    match args.logging:
        case 'critical':
            logger.basicConfig(format=FORMAT, level=logger.CRITICAL)
        case 'error':
            logger.basicConfig(format=FORMAT, level=logger.ERROR)
        case 'warning':
            logger.basicConfig(format=FORMAT, level=logger.WARNING)
        case 'info':
            logger.basicConfig(format=FORMAT, level=logger.INFO)
        case 'debug':
            logger.basicConfig(format=FORMAT, level=logger.DEBUG)

    if args.MPI > 1:
        raise NotImplementedError("Not implemented in this version")
        # logging.debug(f"Running MPI with {args.MPI} processes")
        # kinda janky, should probably not even do that
        proc_args = [
            "mpiexec", "-n", str(args.MPI), 
            "python", "nhts-mpi.py", 
            "-t", str(args.template.absolute()),
            "-s", str(args.seq.absolute()),
            "-o", str(args.output.absolute()),
            "-M", str(args.max_iter)
        ]
        if args.seed:
            proc_args.extend(["-z", str(args.seed)])
        if args.start:
            proc_args.extend(["-S", str(args.start)])
        subprocess.Popen(proc_args, cwd=Path(__file__).parent).wait()
    else:
        logger.debug(f"Running serial algorithm")
        t0 = time.time()
        result = serial_search(args)
        t1 = time.time()
        logger.info(f"Runtime: {t1-t0} s")

if __name__ == "__main__":
    main()
